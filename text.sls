;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-edit text)
  (export
    make-text
    text-size
    text-filename
    text-set-filename
    text-insert
    text-append
    text-char
    text-string
    text-lines-count
    text-line-number
    text-delete
    text-replace
    text-snapshot
    text-undo
    text-redo
    text-modified?
    text-save
    text-add-property
    text-delete-property
    text-delete-all-properties
    text-for-each-property
    make-cursor
    with-saved-cursor
    cursor-x
    cursor-y
    cursor-set-x
    cursor-set-y
    cursor-to
    cursor-to-next-char
    cursor-to-prev-char
    cursor-to-line-begin
    cursor-to-line-end
    cursor-line-begin-pos
    cursor-line-end-pos
    cursor-pos
    cursor-update
    cursor-eof?
    cursor-has-next-char?
    cursor-has-prev-char?)
  (import
    (rnrs)
    (rnrs mutable-strings)
    (text-edit property))

(define EPOS (greatest-fixnum))

(define (string-copy! src src-start dst dst-start n)
  (let loop ([src-idx src-start][dst-idx dst-start][count n])
    (when (> count 0)
      (string-set! dst dst-idx (string-ref src src-idx))
      (loop (+ src-idx 1) (+ dst-idx 1) (- count 1) ))))

(define (copy-data src src-start dst dst-start n)
  (string-copy! src src-start dst dst-start n))

(define (make-data n)
  (make-string n))

(define (data-length data)
  (string-length data))

(define-record-type block
  (fields
    (mutable size)
    (mutable capacity)
    data))

(define (block-new len)
  (make-block 0 len (make-data len)))

(define (block-can-store? blk size)
  (<= size (block-capacity blk)))

(define (block-can-delete? blk offset len)
  (<= (+ offset len) (block-size blk)))

(define (block-delete blk offset len)
  (if (not (= (+ offset len)
              (block-size blk)))
      (copy-data (block-data blk) (+ offset len)
                 (block-data blk) offset
                 (- (block-size blk) len)))
  (block-capacity-set! blk (+ (block-capacity blk) len))
  (block-size-set! blk (- (block-size blk) len)))

(define (block-append blk data len)
    (let ([pos (block-size blk)])
      (block-capacity-set! blk (- (block-capacity blk) len))
      (block-size-set! blk (+ (block-size blk) len))
      (copy-data data 0 (block-data blk) pos len)
      pos))

(define (block-insert blk data offset len)
  (let ([tmp (substring (block-data blk) offset (block-size blk))])
    (copy-data tmp 0 (block-data blk) (+ offset len) (string-length tmp))
    (copy-data data 0 (block-data blk) offset len)
    (block-capacity-set! blk (- (block-capacity blk) len))
    (block-size-set! blk (+ (block-size blk) len))
  )
)

(define-record-type piece
  (fields
    (mutable prev)
    (mutable next)
    (mutable block)
    (mutable offset) ;; offset within a block
    (mutable size)   ;; in bytes
    ))

(define (piece-init piece prev next block offset size)
  (piece-prev-set! piece prev)
  (piece-next-set! piece next)
  (piece-block-set! piece block)
  (piece-offset-set! piece offset)
  (piece-size-set! piece size))

(define (piece-char piece offset)
  (string-ref (block-data (piece-block piece))
              (+ (piece-offset piece) offset)))

(define-record-type span
  (fields
    start
    end
    (mutable size))
  (protocol
    (lambda (new)
      (lambda (start end)
        (and start end
          (let loop ([curr start] [size (piece-size start)])
            (if (eq? curr end)
              (new start end size)
              ;; else
              (let ([next (piece-next curr)])
                (loop next (+ (piece-size next)
                              size)) ))))))))

(define (span-swap txt old new)
  (define (replace-piece start end new-prev new-next)
    (let ([start-prev (piece-prev start)]
          [end-next (piece-next end)])
      (piece-next-set! start-prev new-prev)
      (piece-prev-set! end-next new-next) ))

  (when (or old new)
    (cond
      [(eq? old #f)
       (replace-piece (span-start new) (span-end new)
                      (span-start new) (span-end new)) ]
      [(eq? new #f)
       (replace-piece (span-start old) (span-end old)
                      (piece-next (span-end old)) (piece-prev (span-start old))) ]
      [else
       (replace-piece (span-start old) (span-end old)
                      (span-start new) (span-end new)) ])
    (when old
      ($text-size-set! txt (- ($text-size txt)
                              (span-size old))))
    (when new
      ($text-size-set! txt (+ ($text-size txt)
                              (span-size new)))) ))

(define (span-has-piece? span piece)
  (let loop ([curr (span-start span)])
    (cond
      [(eq? curr #f) #f]
      [(eq? curr piece) #t]
      [(eq? curr (span-end span)) #f]
      [else (loop (piece-next curr))]) ))

(define-record-type change
  (fields
    (mutable old-span)
    (mutable new-span)
    pos
    ))

(define-record-type revision
  (fields
    (mutable seq)
    (mutable prev)
    (mutable next)
    (mutable changes)
    (mutable current-change)
    ))

(define (revision-undo txt rev)
  (let ([pos #f])
    (for-each
      (lambda (c)
        (span-swap txt (change-new-span c) (change-old-span c))
        (set! pos (change-pos c)))
      (reverse (revision-changes rev)))
    pos))

(define (revision-redo txt rev)
  (let ([pos #f])
    (for-each
      (lambda (c)
        (let* ([new-span (change-new-span c)]
               [old-span (change-old-span c)]
               [new-size (if new-span (span-size new-span) 0)]
               [old-size (if old-span (span-size old-span) 0)])
          (span-swap txt old-span new-span)
          (set! pos (change-pos c))
          (when (> new-size old-size)
            (set! pos (+ pos (- new-size old-size))))))
      (reverse (revision-changes rev)))
      pos))

(define-record-type cursor
  (fields
    (mutable x cursor-x cursor-set-x)
    (mutable y cursor-y cursor-set-y)
    (mutable pos)
    (mutable piece)
    (mutable offset)
    (immutable text))
  (protocol
    (lambda (new)
      (lambda (txt)
        (new 0 0 0 #f 0 txt) ))))

(define-syntax with-saved-cursor
  (lambda (x)
    (syntax-case x ()
      [(_ curs e0 e* ...)
       #'(let ([pos (cursor-pos curs)]
               [piece (cursor-piece curs)]
               [offset (cursor-offset curs)])
           (dynamic-wind
             (lambda () #f)
             (lambda () e0 e* ... )
             (lambda ()
               (cursor-pos-set! curs pos)
               (cursor-piece-set! curs piece)
               (cursor-offset-set! curs offset))))])))

(define (cursor-to curs to)
  (if (or (negative? to)
          (<= (text-size (cursor-text curs)) 0))
      (let ()
        (cursor-piece-set! curs #f)
        #f)
      ;; else
      (let*([pos (cursor-pos curs)]
            ;;[piece (cursor-piece curs)]
            [piece #f]
            ;;[offset (cursor-offset curs)]
            [offset 0]
            [txt (cursor-text curs)]
            ;;[to-pos (abs (- to pos))]
            [to-pos to]
            [get-piece-func text-get-piece-ext])
        ;;(when (< to pos)
        ;;  (set! get-piece-func text-get-piece-ext-backward)
        ;;  (set! offset (- (piece-size piece) offset))
        ;;  (when piece
        ;;    (set! piece (piece-next piece)) ))
        ;;(set! to-pos (+ to-pos offset))
        (let-values ([(to-piece to-offset) (get-piece-func txt piece to-pos)])
          (if to-piece
              (let ()
                (cursor-piece-set! curs to-piece)
                (cursor-offset-set! curs to-offset)
                (cursor-pos-set! curs to)
                #t)
              ;; else
              (let ()
                (cursor-piece-set! curs ($text-end-piece txt))
                (cursor-offset-set! curs 0)
                (cursor-pos-set! curs (text-size txt))
                #f
              ))))))

(define (cursor-update curs)
  (let ([pos (cursor-pos curs)])
    (cursor-pos-set! curs 0)
    (cursor-piece-set! curs #f)
    (cursor-offset-set! curs 0)
    (cursor-to curs 0)
    (cursor-to curs pos)))

(define (cursor-eof? curs)
  (eqv? (cursor-piece curs)
        ($text-end-piece (cursor-text curs))))

(define (cursor-has-prev-char? curs)
  (let* ([piece (cursor-piece curs)]
         [prev-piece (piece-prev piece)]
         [txt (cursor-text curs)]
         [offs (cursor-offset curs)])
    (and piece
         (or (and prev-piece
                  (not (eqv? prev-piece ($text-start-piece txt))))
             (and (< offs (piece-size piece))
                  (> offs 0) )))))

(define (cursor-has-next-char? curs)
  (let* ([piece (cursor-piece curs)]
         [next-piece (piece-next piece)]
         [txt (cursor-text curs)]
         [offs (cursor-offset curs)])
    (and piece
         (or (and next-piece
                  (not (eqv? next-piece ($text-end-piece txt))))
             (and (< offs (- (piece-size piece) 1))
                  (>= offs 0) )))))

(define cursor-to-next-char
  (case-lambda
    [(curs)
     (cursor-to curs (+ (cursor-pos curs) 1))]

    [(curs ch-or-fn)
     (let loop ()
       (if (cursor-to-next-char curs)
           (cond [(char? ch-or-fn)
                  (if (eq? ch-or-fn (text-char curs))
                      #t
                      ;; else
                      (loop))]
                 [else
                  (if (ch-or-fn curs)
                      #t
                      ;; else
                      (loop))])
           ;; else
           #f))]))

(define cursor-to-prev-char
  (case-lambda
    [(curs)
     (cursor-to curs (- (cursor-pos curs) 1))]

    [(curs ch-or-fn)
     (let loop ()
       (if (cursor-to-prev-char curs)
           (cond [(char? ch-or-fn)
                  (if (eq? ch-or-fn (text-char curs))
                      #t
                      ;; else
                      (loop))]
                 [else
                  (if (ch-or-fn curs)
                      #t
                      ;; else
                      (loop))])
           ;; else
           #f))]))

(define (cursor-to-line-begin curs)
  (or
    (and (cursor-to-prev-char curs #\newline)
         (cursor-to curs (+ (cursor-pos curs) 1)))
    (cursor-update curs)))

(define (cursor-to-line-end curs)
  (or (eqv? #\newline (text-char curs))
      (cursor-to-next-char curs #\newline)
      (cursor-update curs)))

(define (cursor-line-begin-pos curs)
  (with-saved-cursor curs
    (cursor-to-line-begin curs)
    (cursor-pos curs)))

(define (cursor-line-end-pos curs)
  (with-saved-cursor curs
    (cursor-to-line-end curs)
    (cursor-pos curs)))

(define-record-type $text
  (fields
    (mutable rev-count)
    start-piece
    end-piece
    (mutable history)
    (mutable saved-revision)
    (mutable current-revision)
    (mutable current-block)
    (mutable current-piece)
    (mutable blocks)
    (mutable size)
    (mutable filename)
    (mutable cursor-intern)
    (mutable cached-line-num)
    (mutable cached-line-pos)
    props
    ))

(define ($text-add-block txt new-blk)
  ($text-blocks-set! txt (append ($text-blocks txt) (list new-blk)))
  ($text-current-block-set! txt new-blk) )

(define ($text-add-revision txt)
  (let ([rev (make-revision ($text-rev-count txt) #f #f (list) #f)]
        [history ($text-history txt)])
    ($text-current-revision-set! txt rev)
    (when history
      (revision-prev-set! rev history)
      (revision-next-set! history rev))
    ($text-history-set! txt rev)
    ($text-rev-count-set! txt (+ ($text-rev-count txt) 1))
    rev))

(define (text-add-change txt pos)
  (define ($get-revision)
    (or ($text-current-revision txt)
        ($text-add-revision txt)))

  (let ([change (make-change #f #f pos)]
        [rev ($get-revision)])
    (revision-changes-set! rev (append (revision-changes rev) (list change)))
    (revision-current-change-set! rev change)
    change))

(define ($text-store txt data size)
  (define ($get-block)
    (when (or (eq? ($text-current-block txt) #f)
              (>= size
                  (block-capacity ($text-current-block txt))) )
      ($text-add-block txt (block-new 8024)))
    ($text-current-block txt))

  (block-append ($get-block) data size))

(define text-get-piece
  (case-lambda
    [(txt pos)
     (text-get-piece txt ($text-start-piece txt) pos)]

    [(txt piece pos)
     (let loop ([piece ($text-start-piece txt)] [curr-pos 0])
       (let ([next (piece-next piece)]
             [size (piece-size piece)])
         (if (and (<= curr-pos pos)
                  (<= pos (+ curr-pos size)))
           (values piece (- pos curr-pos))
           ;; else
           (if (not next)
             (values #f #f)
             ;; else
             (loop next (+ curr-pos size)) ))))]
             ))

(define text-get-piece-ext
  (case-lambda
    [(txt pos)
     (text-get-piece-ext txt (piece-next ($text-start-piece txt)) pos)]

    [(txt piece pos)
     (let loop ([piece (or piece (piece-next ($text-start-piece txt)))] [curr-pos 0])
       (let ([next (piece-next piece)]
             [size (piece-size piece)])
         (if (and (<= curr-pos pos)
                  (< pos (+ curr-pos size)))
           (values piece (- pos curr-pos))
           ;; else
           (if (eq? next ($text-end-piece txt))
             (if (= pos curr-pos)
                 (let ([prev (piece-prev piece)])
                   (if (not (eq? prev ($text-start-piece txt)))
                     (values prev (piece-size prev)))
                     ;; else
                     (values #f #f))
                 ;; else
                 (values #f #f))
             ;; else
             (if next
               (loop next (+ curr-pos size))
               (values #f #f)) ))))] ))

(define text-get-piece-ext-backward
  (case-lambda
    [(txt pos)
     (text-get-piece-ext txt (piece-prev ($text-end-piece txt)) pos)]

    [(txt piece pos)
     (let loop ([piece (or piece ($text-end-piece txt))] [curr-pos 0])
       (let ([prev (piece-prev piece)])
         (if (and prev
                  (<= curr-pos pos)
                  (< pos (+ curr-pos (piece-size prev))))
           (values prev (- (piece-size prev) (- pos curr-pos)))
           ;; else
           (if (eq? prev ($text-start-piece txt))
             (if (= pos curr-pos)
                 (let ([next (piece-next prev)])
                   (if (not (eq? next ($text-end-piece txt)))
                       (values next 0)
                       ;; else
                       (values #f #f)))
                 ;; else
                 (values #f #f))
             ;; else
             (if prev
               (loop prev (+ curr-pos (piece-size prev)))
               (values #f #f)) ))))] ))

(define make-text
  (case-lambda
    [()
     (make-text #f)
    ]

    [(filename)
     (let* ([start-piece (make-piece #f #f #f 0 0)]
            [end-piece   (make-piece #f #f #f 0 0)]
            [prev-piece start-piece]
            [next-piece end-piece])
       (let ([txt (make-$text 0 start-piece end-piece #f #f #f #f #f (list) 0 #f #f 0 0 (make-text-property-list))])
         (when filename
           (let*([str (with-input-from-file filename
                                            (lambda ()
                                              (get-string-all (current-input-port))))]
                 [str (if (eof-object? str) "" str)]
                 [block (make-block (string-length str) 0 str)]
                 [piece (make-piece #f #f block 0 (string-length str))])
             ($text-add-block txt block)
             (piece-prev-set! piece start-piece)
             (piece-next-set! piece end-piece)
             (set! next-piece piece)
             (set! prev-piece piece)
             ($text-filename-set! txt filename)
             ($text-size-set! txt (piece-size piece))))
         (piece-next-set! start-piece next-piece)
         (piece-prev-set! end-piece prev-piece)
         (text-add-change txt EPOS)
         (text-snapshot txt)
         ($text-saved-revision-set! txt ($text-history txt))
         ($text-cursor-intern-set! txt (make-cursor txt))
         txt))]
  ))

(define (text-snapshot txt)
  ($text-current-revision-set! txt #f)
  ($text-current-piece-set! txt #f))

(define (text-undo txt)
  (text-snapshot txt)
  (let ([rev (revision-prev ($text-history txt))])
    (if rev
        (let ([pos (revision-undo txt ($text-history txt))])
          (text-line-cache-invalidate! txt)
          ($text-history-set! txt rev)
          pos)
        #f)))

(define (text-redo txt)
  (text-snapshot txt)
  (let ([rev (revision-next ($text-history txt))])
    (if rev
        (let ([pos (revision-redo txt rev)])
          (text-line-cache-invalidate! txt)
          ($text-history-set! txt rev)
          pos)
        #f)))

(define (text-modified? txt)
  (eqv? ($text-saved-revision txt) ($text-history txt)))

(define (cache-piece txt piece)
  (let ([curr-block ($text-current-block txt)])
    (when (and curr-block
               (eq? (piece-block piece) curr-block)
               (= (+ (piece-offset piece) (piece-size piece))
                  (block-size curr-block)))
      ($text-current-piece-set! txt piece))))

(define (cache-contains? txt piece)
  (if (and ($text-current-block txt)
           (eq? piece ($text-current-piece txt))
           ($text-current-revision txt)
           (revision-current-change ($text-current-revision txt)))
      (let* ([curr-block ($text-current-block txt)]
             [rev ($text-current-revision txt)]
             [ch (revision-current-change rev)]
             [new-span (change-new-span ch)])
        (and (span-has-piece? new-span piece)
             (= (+ (piece-offset piece) (piece-size piece))
                (block-size curr-block))))
      ;; else
      #f))

(define (cache-insert txt piece offset data size)
  (if (and (cache-contains? txt piece)
           (block-can-store? (piece-block piece) size))
      (let ([new-span (change-new-span (revision-current-change ($text-current-revision txt)))]
            [offset (+ (piece-offset piece) offset)]
            [block (piece-block piece)])
        (if (= offset (block-size block))
            (block-append block data size)
          ;; else
          (block-insert block data offset size))
        (piece-size-set! piece (+ (piece-size piece) size))
        ($text-size-set! txt (+ ($text-size txt) size))
        (span-size-set! new-span (+ (span-size new-span) size))
        #t)
      ;; else
      #f))

(define (cache-delete txt piece offset size)
  (if (and (cache-contains? txt piece)
           (block-can-delete? (piece-block piece) (+ (piece-offset piece) offset) size))
      (let ([new-span (change-new-span (revision-current-change ($text-current-revision txt)))])
        (block-delete (piece-block piece) (+ (piece-offset piece) offset) size)
        (piece-size-set! piece (- (piece-size piece) size))
        ($text-size-set! txt (- ($text-size txt) size))
        (span-size-set! new-span (- (span-size new-span) size))
        #t)
      ;; else
      #f))

(define (text-line-cache-invalidate! txt)
  ($text-cached-line-num-set! txt 0)
  ($text-cached-line-pos-set! txt 0))

(define (text-insert txt str pos)
  (define (get-data)
    str)

  (when (> pos ($text-size txt))
    (error 'text-insert "Position is over text size"))
  (let-values ([(piece offset) (text-get-piece txt pos)])
    (when (not piece)
      (error 'text-insert (string-append "Invalid position " (number->string pos))))
    (when (< pos ($text-cached-line-pos txt))
      (text-line-cache-invalidate! txt))
    (let* ([data (get-data)]
           [size (data-length data)])
      (if (not (cache-insert txt piece offset data size))
          (let* ([blk-pos ($text-store txt data size)]
                 [prev-piece (piece-prev piece)]
                 [next-piece (piece-next piece)]
                 [change (text-add-change txt pos)])
            (if (= offset (piece-size piece))
              (let ([new-piece (make-piece piece next-piece ($text-current-block txt) blk-pos size)])
                (change-new-span-set! change (make-span new-piece new-piece))
                (cache-piece txt new-piece))

              ;; else break piece in the middle by 3 new pieces
              (let ([before-piece (make-piece #f #f #f 0 0)]
                    [after-piece (make-piece #f #f #f 0 0)]
                    [new-piece (make-piece #f #f #f 0 0)])
                (piece-init before-piece prev-piece new-piece (piece-block piece) (piece-offset piece) offset)
                (piece-init new-piece before-piece after-piece ($text-current-block txt) blk-pos size)
                (piece-init after-piece new-piece next-piece (piece-block piece) (+ (piece-offset piece)
                                                                                    offset)
                                                                                 (- (piece-size piece)
                                                                                    offset))
                (change-new-span-set! change (make-span before-piece after-piece))
                (change-old-span-set! change (make-span piece piece))
                (cache-piece txt new-piece)))
            (span-swap txt (change-old-span change) (change-new-span change))
            ))) ))

(define (text-append txt str)
  (text-insert txt str (text-size txt)))

(define (text-size txt)
  ($text-size txt))

(define (text-filename txt)
  ($text-filename txt))

(define (text-set-filename txt filename)
  ($text-filename-set! txt filename))

(define (copy-bytes! txt start-piece poffset size bv-data bv-start)
  (define copied-count 0)

  (let loop ([piece start-piece] [curr-pos 0])
    (cond
      [(and (< curr-pos size)
            (not (eq? piece ($text-end-piece txt))))
       (let* ([first-piece? (eq? piece start-piece)]
              [last-piece? (>= (+ curr-pos (piece-size piece))
                               size)]
              [block (piece-block piece)]
              [blk-pos (piece-offset piece)]
              [next (piece-next piece)]
              [psize (piece-size piece)]
              [start-offset (cond [first-piece? poffset] [else 0])]
              [real-size (cond
                           [(and first-piece? last-piece?) (min size (- psize poffset))]
                           [first-piece? (- psize poffset)]
                           [last-piece?  (min psize (- size curr-pos))]
                           [else psize]) ])
         (when (> psize 0)
           (copy-data (block-data block) (+ blk-pos start-offset)
                      bv-data (+ bv-start curr-pos)
                      real-size)
           (set! copied-count (+ copied-count real-size)))
         (loop next (+ curr-pos real-size)) )]
      [else copied-count] )))

(define (get-data txt start-piece poffset size)
  (if (> size 0)
      (let*([bv (make-data size)]
            [count (copy-bytes! txt start-piece poffset size bv 0)])
        (if (not (= count size))
            (let ([new-bv (make-data count)])
              (copy-data bv 0 new-bv 0 count)
              new-bv)
            ;; else
            bv))
      ;; else
      (make-data 0)))

(define (get-string txt piece pos size)
  (if piece
      (get-data txt piece pos size)
      ;; else
      ""))

(define text-char
  (case-lambda
     [(curs)  
      (and (cursor-piece curs)
           (not (eqv? ($text-end-piece (cursor-text curs))
                      (cursor-piece curs)))
           (piece-char (cursor-piece curs) (cursor-offset curs)) )]

     [(txt pos)
      (and (< pos (text-size txt))
           (let-values ([(piece offset) (text-get-piece-ext txt pos)])
             (and piece (piece-char piece offset) )))] ))

(define text-string
  (case-lambda
    [(txt)
     (text-string txt 0 (text-size txt))]

    [(curs size)
     (when (not (cursor-piece curs))
       (cursor-to curs (cursor-pos curs)))
     (get-string (cursor-text curs) (cursor-piece curs) (cursor-offset curs) size)]

    [(txt pos size)
     (let-values ([(piece offset) (text-get-piece-ext txt pos)])
       (get-string txt piece offset size) )] ))

;; counts in range of [pos, pos+len)
(define (text-lines-count txt pos len)
  (let ([curs ($text-cursor-intern txt)]
        [len (min len (text-size txt))]
        [count 0])
    (cursor-update curs)
    (cursor-to curs pos)

    (let loop ([pos pos] [curr-len 0])
      (when (cursor-to-line-end curs)
        (set! curr-len (+ curr-len (- (cursor-pos curs) pos)))
        (when (< curr-len len)
          (set! count (+ count 1))
          (cursor-to-next-char curs)
          (set! curr-len (+ curr-len 1))
          (when (< curr-len len)
            (loop (cursor-pos curs) curr-len)) )))
    count))

;;(define (text-line-number txt pos)
;;  (define line-pos ($text-cached-line-pos txt))
;;  (define line-num ($text-cached-line-num txt))
;;  (define curs ($text-cursor-intern txt))
;;
;;  (define (lines-count to-pos len)
;;    (let ([count (text-lines-count txt to-pos len)])
;;      count))
;;
;;  (cursor-update curs)
;;  (cursor-to curs pos)
;;
;;  (when (> pos (text-size txt))
;;    (set! pos (text-size txt)))
;;  (cond [(< pos line-pos)
;;         (let ([diff (- line-pos pos)])
;;           (if (< diff pos)
;;               (set! line-num (- line-num (lines-count pos diff)))
;;               ;; else
;;               (set! line-num (lines-count 0 pos)) ) )]
;;
;;        [(> pos line-pos)
;;         (set! line-num (+ line-num (lines-count line-pos (- pos line-pos)))) ])
;;  (cursor-to curs pos)
;;  (cursor-to-line-begin curs)
;;  ($text-cached-line-num-set! txt line-num)
;;  ($text-cached-line-pos-set! txt (cursor-pos curs))
;;  line-num)

(define (text-line-number txt pos)
  (text-lines-count txt 0 pos))

(define (text-delete txt pos len)
  (when (> (+ pos len) (text-size txt))
    (error 'text-delete "Pos + len is bigger than text size"))
  (when (< pos ($text-cached-line-pos txt))
    (text-line-cache-invalidate! txt))
 
  (let-values ([(piece offset) (text-get-piece txt pos)])
    (if (not (cache-delete txt piece offset len))
    (let* ([change (text-add-change txt pos)]
           [midway-start? #f]
           [midway-end? #f]
           [curr-pos 0]
           [before #f]
           [after #f]
           [start #f]
           [end #f]
           [new-start #f]
           [new-end #f])
      (cond [(= offset (piece-size piece))
             ;; deletion starts at piece boundary
             (set! before piece)
             (set! start (piece-next piece))]
            [else
             ;; deletion starts midway within the piece
             (set! midway-start? #t)
             (set! curr-pos (- (piece-size piece) offset))
             (set! start piece)
             (set! before (make-piece #f #f #f 0 0)) ])

      ;; skip all pieces which fall into deletion range
      (let loop ()
        (when (< curr-pos len)
          (set! piece (piece-next piece))
          (set! curr-pos (+ curr-pos (piece-size piece)))
          (loop) ))

      (cond [(= curr-pos len)
             ;; deletion stops at piece boundary
             (set! end piece)
             (set! after (piece-next piece))
            ]
            [else
             ;; cur-pos > len: deletion stops midway through a piece */
             (set! midway-end? #t)
             (set! end piece)
             (set! after (make-piece #f #f #f 0 0))
             (piece-init after before (piece-next piece)
                         (piece-block piece)
                         (+ (piece-offset piece)
                            (- (piece-size piece)
                               (- curr-pos len)))
                         (- curr-pos len)) ])
      (when midway-start?
        (piece-init before (piece-prev start) after (piece-block start) (piece-offset start) offset)
        (set! new-start before)
        (when (not midway-end?)
          (set! new-end before) ))
      (when midway-end?
        (set! new-end after)
        (when (not midway-start?)
          (set! new-start after) ))
      (change-new-span-set! change (make-span new-start new-end))
      (change-old-span-set! change (make-span start end))
      (span-swap txt (change-old-span change) (change-new-span change))
      ))))

(define (text-replace txt pos len str)
  (text-delete txt pos len)
  (text-insert txt str pos))

(define (text-save txt)
  (let ([fp (open-file-output-port (text-filename txt)
                                   (file-options no-fail)
                                   (buffer-mode block)
                                   (native-transcoder))])
    (put-string fp (text-string txt))
    (close-port fp)
    ($text-saved-revision-set! txt ($text-history txt))
    (text-snapshot txt) ))

(define (text-debug txt)
  (display "[")
  (display "revision: ")(display (revision-seq ($text-history txt))) (display ", ") 
  (display "text: ")(display (text-string txt))
  (display "]")
  (newline)
)

(define (text-add-property txt start end data)
  (add-text-property ($text-props txt) start end data))

(define (text-delete-property txt start end)
  (delete-text-property ($text-props txt) start end))

(define (text-delete-all-properties txt start end)
  (text-delete-property txt 0 (text-size txt)))

(define (text-for-each-property txt start end fn)
  (for-each-text-property ($text-props txt) start end fn))

)
