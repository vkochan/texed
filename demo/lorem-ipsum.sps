#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 vkochan
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (text-mode console)
        (text-mode console events)
        (text-mode termios)
        (text-edit text)
        (text-edit view))

(define max-height 10)
(define max-width 80)
(define text-y 1)

(define str
"Lorem Ipsum is \tsimply dummy text of the printing and
typesetting industry. Lorem Ipsum has been the industry's
standard dummy text \tever since the 1500s, when an unknown
printer took a galley of type and scrambled it to make a
type specimen book. It has survived not only five centuries,
but also the leap into electronic typesetting, remaining
essentially unchanged. It was popularised in the 1960s
with the release of Letraset sheets containing Lorem Ipsum
passages, and more recently with desktop publishing software
like Aldus PageMaker including versions of Lorem Ipsum.

Contrary to popular belief, Lorem Ipsum is not simply
random text. It has roots in a piece of classical Latin
literature from 45 BC, making it over 2000 years old.
Richard McClintock, a Latin professor at Hampden-Sydney
College in Virginia, looked up one of the more obscure
Latin words, consectetur, from a Lorem Ipsum passage,
and going through the cites of the word in classical
literature, discovered the undoubtable source. Lorem
Ipsum comes from sections 1.10.32 and 1.10.33 of
\"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil)
by Cicero, written in 45 BC. This book is a treatise on the theory
of ethics, very popular during the Renaissance. The first line of
Lorem Ipsum, \"Lorem ipsum dolor sit amet..\",
comes from a line in section 1.10.32.

It is a long established fact that a reader will be
distracted by the readable content of a page when
looking at its layout. The point of using Lorem
Ipsum is that it has a more-or-less normal distribution
of letters, as opposed to using 'Content here, content
here', making it look like readable English. Many desktop
publishing packages and web page editors now use Lorem
Ipsum as their default model text, and a search for 'lorem ipsum'
will uncover many web sites still in their infancy. Various
versions have evolved over the years, sometimes by accident,
sometimes on purpose (injected humour and the like).")

(define txt (make-text))
(define vw (make-view txt max-width max-height))
(define ch-tbl (make-char-table
'(
  (#\tab (width 4) (fill #\space))
)))

(define curs (make-cursor txt))
(define curs-x 0)
(define curs-y 0)

(view-set-char-table vw ch-tbl)
(text-insert txt str 0)

(cursor-to curs 0)

(define (move-left)
  (when (> (cursor-pos curs) 0)
    (cursor-to-prev-char curs)
    (when (< (cursor-pos curs) (view-start-pos vw))
      (view-scroll-up vw 1) )))

(define (move-right)
  (when (< (cursor-pos curs) (- (text-size txt) 1))
    (cursor-to-next-char curs)
    (when (> (cursor-pos curs) (view-end-pos vw))
      (view-scroll-down vw 1) )))

(define (move-down)
  (let-values ([(l c p) (view-at vw (cursor-pos curs))])
    (if (and l (= l (- (view-height vw) 1)))
        (begin
          (view-scroll-down vw 1)
          (let-values ([(l2 c2 p2) (view-at-char vw l c)])
            (when l2
              (cursor-to curs p2) )))
      (let-values ([(l2 c2 p2) (view-at-char vw (+ l 1) c)])
        (when l2
          (cursor-to curs p2)))
    )))

(define (move-up)
  (let-values ([(l c p) (view-at vw (cursor-pos curs))])
    (if (and l (= l 0))
        (begin
          (view-scroll-up vw 1)
          (let-values ([(l2 c2 p2) (view-at-char vw l c)])
            (when l2
              (cursor-to curs p2) )))
      (let-values ([(l2 c2 p2) (view-at-char vw (- l 1) c)])
        (when l2
          (cursor-to curs p2)))
    )))

(define (delete-prev-char)
  (when (> (cursor-pos curs) 0)
    (cursor-to-prev-char curs)
    (text-delete txt (cursor-pos curs) 1)
    (cursor-update curs)
    (view-draw vw)))

(define (insert-char ch)
  (text-insert txt (make-string 1 ch) (cursor-pos curs))
  (cursor-update curs)
  (cursor-to-next-char curs)
  (view-draw vw)
  (when (> (cursor-pos curs) (view-end-pos vw))
    (view-scroll-down vw 1)) )

(define (debug m)
  (let ([x (wherex)]
        [y (wherey)])
    (gotoxy 0 (window-maxy))
    (print m)
    (gotoxy x y) ))

(define (fill-line x y ch)
  (let loop ([x x] [n (- (window-maxx) x)])
    (when (> n 0)
      (gotoxy x y)
      (print ch)
      (loop (+ x 1) (- n 1)) )))

(define (draw-header)
  (gotoxy 0 0)
  (clreol)
  (text-color White)
  (text-background Red)
  (print "Demo of texed library")
  (fill-line (wherex) (wherey) " ")
)

(define (draw-footer)
  (gotoxy 0 (+ max-height 1))
  (clreol)
  (text-color White)
  (text-background Red)

  (clreol)
  (print "pos:")(print (cursor-pos curs))(print ", line:")(print curs-y)(print ", col:")(print curs-x)
  (fill-line (wherex) (wherey) " ")
  
  (gotoxy 0 (+ max-height 2))
  (clreol)
  (print "[q:Exit] [C-d:Scroll down] [C-u:Scroll up] [PageUp:Scroll page up] [PageDown:Scroll page down]")
  (fill-line (wherex) (wherey) " ")

  (gotoxy 0 (+ max-height 3))
  (clreol)
  (print "[Alt-h:Cursor left] [Alt-j:Cursor down] [Alt-k:Cursor up] [Alt-l:Cursor left]")
  (fill-line (wherex) (wherey) " ")
)

(define (draw-text)
  (text-color Green)
  (text-background Black)
  
  (gotoxy 0 text-y)

  (view-for-each-cell vw
    (lambda (line col cell pos)
      (gotoxy col (+ (line-row line) text-y))
      (let ([ch (cell-char cell)])
        (cond
          [(eqv? ch #\newline)
           (print #\space)]
          [else
           (print ch)])
      ))))

(define (update-cursor)
  (when (or (< (cursor-pos curs) (view-start-pos vw))
            (> (cursor-pos curs) (view-end-pos vw)))
    (cursor-to curs (view-start-pos vw)))
  (let-values ([(y x p) (view-at vw (cursor-pos curs))])
    (when (and y x)
      (set! curs-x x)
      (set! curs-y y))))

(define (draw-cursor)
  (gotoxy curs-x (+ curs-y 1)) )

(define (draw-ui)
  (draw-header)
  (draw-text)
  (update-cursor)
  (draw-footer)
  (draw-cursor)
)

(clrscr)
(redraw-screen)

(view-draw vw)
(draw-ui)

(let loop ()
  (update-screen)
  (let ((ev (read-event)))
    (when (not (and (key-press-event? ev)
                    (eqv? (keyboard-event-key ev) #\q)))
      (when (key-press-event? ev)
        (cond
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set ctrl))
                (eqv? (keyboard-event-key ev) #\d))
           (view-scroll-down vw 1)]
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set ctrl))
                (eqv? (keyboard-event-key ev) #\u))
           (view-scroll-up vw 1)]
          [(eqv? (keyboard-event-key ev) 'PageUp)
           (view-scroll-page-up vw)]
          [(eqv? (keyboard-event-key ev) 'PageDown)
           (view-scroll-page-down vw)]
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set alt))
                (eqv? (keyboard-event-key ev) #\h))
           (move-left)]
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set alt))
                (eqv? (keyboard-event-key ev) #\l))
           (move-right)]
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set alt))
                (eqv? (keyboard-event-key ev) #\j))
           (move-down)]
          [(and (enum-set=? (keyboard-event-mods ev)
                            (modifier-set alt))
                (eqv? (keyboard-event-key ev) #\k))
           (move-up)]
          [(eqv? (keyboard-event-key ev) 'Backspace)
           (delete-prev-char)]
          [(and (not (keyboard-event-has-modifiers? ev))
                (keyboard-event-char ev))
           (insert-char (keyboard-event-char ev))])
           
        (gotoxy 0 (+ max-height 4))
        (clreol)
        (print (keyboard-event-key ev))
        (draw-ui))
      (loop) )))

(restore-console)
