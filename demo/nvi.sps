#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (text-mode console)
        (text-mode console events)
        (text-mode termios)
        (text-edit text)
        (text-edit view)
        (text-edit editor)
        (text-edit keymap))

(define max-height (- (window-maxy) 1))
(define max-width (window-maxx))
(define text-x-pad 1)
(define text-y 0)

(define file (and (> (length (command-line)) 1)
                  (list-ref (command-line) 1)))

(define ed (make-editor max-width max-height file))

(define editor-list (list ed))
(define editor-id 0)

(define (current-editor)
  (list-ref editor-list editor-id))

(define (current-mark)
  (editor-mark (current-editor)))

(define (current-cursor)
  (editor-cursor (current-editor)))

(define (current-text)
  (editor-text (current-editor)))

(define (current-view)
  (editor-view (current-editor)))

(define copybuf-text "")

(define (visual-line-fixup)
   (if (>= (cursor-pos (current-cursor)) (cursor-pos (current-mark)))
       (begin
         (cursor-to-line-begin (current-mark))
         (cursor-to-line-end (current-cursor)))
       (begin
         (cursor-to-line-begin (current-cursor))
         (cursor-to-line-end (current-mark)))))

(define (selection-delete)
  (set! current-kmap normal-kmap)
  (editor-delete-selection ed)
  (text-snapshot (current-text)))

(define (selection-copy)
  (let-values ([(start-pos end-pos) (editor-selected-range (current-editor))])
    (set! copybuf-text (text-string (current-text) start-pos (+ (- end-pos start-pos) 1)))
    (set! current-kmap normal-kmap)
    (view-set-dirty (current-view) #t)))

(define (copybuf-paste)
  (when (positive? (string-length copybuf-text))
    (cursor-to-line-end (current-cursor))
    (cursor-to-next-char (current-cursor))
    (editor-insert-string ed copybuf-text)
    (text-snapshot (current-text))))

(define main-kmap (make-keymap))

(bind-key main-kmap '(alt q) (lambda () (set! quit? #t)))

(define visual-line-kmap (make-keymap main-kmap))
(bind-key visual-line-kmap '(j) (lambda ()
                                  (editor-move-cursor-down (current-editor))
                                  (visual-line-fixup)))
(bind-key visual-line-kmap '(k) (lambda ()
                                  (editor-move-cursor-up (current-editor))
                                  (visual-line-fixup)))
(bind-key visual-line-kmap '(d) (lambda () (selection-delete)))
(bind-key visual-line-kmap '(y) (lambda () (selection-copy)))
(bind-key visual-line-kmap '(Escape) (lambda ()
                                       (set! current-kmap normal-kmap)))

(define normal-kmap (make-keymap main-kmap))
(bind-key normal-kmap '(ArrowLeft) (lambda () (editor-move-cursor-left (current-editor))))
(bind-key normal-kmap '(ArrowRight) (lambda () (editor-move-cursor-right (current-editor))))
(bind-key normal-kmap '(ArrowDown) (lambda () (editor-move-cursor-down (current-editor))))
(bind-key normal-kmap '(ArrowUp) (lambda () (editor-move-cursor-up (current-editor))))
(bind-key normal-kmap '(h) (lambda () (editor-move-cursor-left (current-editor))))
(bind-key normal-kmap '(j) (lambda () (editor-move-cursor-down (current-editor))))
(bind-key normal-kmap '(k) (lambda () (editor-move-cursor-up (current-editor))))
(bind-key normal-kmap '(l) (lambda () (editor-move-cursor-right (current-editor))))
(bind-key normal-kmap '(x) (lambda ()
                             (editor-move-cursor-right (current-editor))
                             (editor-delete-char (current-editor))
                             (text-snapshot (current-text))))
(bind-key normal-kmap '(shift x) (lambda ()
                                   (editor-delete-char (current-editor))
                                   (text-snapshot (current-text))))
(bind-key normal-kmap '(d d) (lambda ()
                               (editor-delete-line (current-editor))
                               (text-snapshot (current-text))))
(bind-key normal-kmap '(p) (lambda () (copybuf-paste)))
(bind-key normal-kmap '(0) (lambda () (cursor-to-line-begin (current-cursor))))
(bind-key normal-kmap '($) (lambda () (cursor-to-line-end (current-cursor))))
(bind-key normal-kmap '(shift v) (lambda ()
                                   (set! current-kmap visual-line-kmap)
                                   (cursor-update (current-mark))
                                   (cursor-to (current-mark) (cursor-pos (current-cursor)))
                                   (visual-line-fixup)))
(bind-key normal-kmap '(i) (lambda () (set! current-kmap insert-kmap)))
(bind-key normal-kmap '(shift i) (lambda ()
                                   (set! current-kmap insert-kmap)
                                   (cursor-to-line-begin (current-cursor))))
(bind-key normal-kmap '(shift a) (lambda ()
                                   (set! current-kmap insert-kmap)
                                   (cursor-to-line-end (current-cursor))))
(bind-key normal-kmap '(g g) (lambda ()
                               (cursor-to (current-cursor) 0)
                               (view-set-start-pos (current-view) 0)))
(bind-key normal-kmap '(shift g) (lambda ()
                                   (cursor-to (current-cursor) (text-size (current-text)))
                                   (view-set-start-pos (current-view) (text-size (current-text)))
                                   (view-scroll-page-up (current-view))))
(bind-key normal-kmap '(PageUp) (lambda () (view-scroll-page-up (current-view))))
(bind-key normal-kmap '(PageDown) (lambda () (view-scroll-page-down (current-view))))
(bind-key normal-kmap '(ctrl s) (lambda () (text-save (current-text))))
(bind-key normal-kmap '(ctrl u) (lambda () (view-scroll-page-up (current-view))))
(bind-key normal-kmap '(ctrl d) (lambda () (view-scroll-page-down (current-view))))
(bind-key normal-kmap '(u) (lambda () (editor-undo (current-editor))))
(bind-key normal-kmap '(ctrl r) (lambda () (editor-redo (current-editor))))

(define insert-kmap (make-keymap main-kmap))
(bind-key insert-kmap '(Escape) (lambda ()
                                  (set! current-kmap normal-kmap)
                                  (text-snapshot (current-text))))
(bind-key insert-kmap '(Backspace) (lambda () (editor-delete-char (current-editor))))
(bind-key insert-kmap '(Enter) (lambda () (editor-insert-char (current-editor) #\newline)))

(define current-kmap normal-kmap)

(define keys-buf '())

(define char-tbl (make-char-table
'(
  (#\tab (width 4) (fill #\space))
)))

(define sidebar-width 2)

(view-set-char-table (current-view) char-tbl)
(view-enable-line-numbering (current-view) #t)
(view-set-default-cell (current-view) (make-cell Green Black #f))

(define (insert-char ch)
  (when (eqv? current-kmap insert-kmap)
    (editor-insert-char (current-editor) ch)))

(define line-num-changed? #t)
(define line-num-cached 0)

(define (line-num-width)
  (let ([num (text-line-number (current-text) (view-end-pos (current-view)))])
    (set! line-num-changed? (not (fx=? line-num-cached num)))
    (set! line-num-cached num)
    (string-length (number->string num))))

(define (draw-text)
  (define prev-line-num #f)

  (set! sidebar-width (+ (line-num-width) 1))

  (when line-num-changed?
    (view-resize
      (current-view) (fx- max-width (+ sidebar-width text-x-pad))
		     max-height))
  (text-color Default)
  (text-background Default)
  
  (gotoxy 0 text-y)

  (view-for-each-line (current-view)
    (lambda (l)
      (gotoxy 0 (+ text-y (line-row l)))
      (clreol)
      (when (and (line-number l) (not (eq? prev-line-num (line-number l))))
        (set! prev-line-num (line-number l))
        (print (number->string (+ (line-number l) 1))) )
      (gotoxy (- sidebar-width 1) (+ text-y (line-row l)))
      (print "|") ))

  (when (eqv? current-kmap visual-line-kmap)
    (let-values ([(start-pos end-pos) (editor-selected-range (current-editor))])
      (view-for-each-cell (current-view) start-pos end-pos
        (lambda (line col cell pos)
          (cell-set-fg-color cell White)
          (cell-set-bg-color cell Blue)))))

  (view-for-each-cell (current-view)
    (lambda (line col cell pos)
      (gotoxy (+ col text-x-pad sidebar-width) (+ (line-row line) text-y))
      (let ([ch (cell-char cell)]
            [fg (cell-fg-color cell)]
            [bg (cell-bg-color cell)])
        (text-color (cell-fg-color cell))
        (text-background (cell-bg-color cell))
        (cond
          [(eqv? ch #\newline)
           (print #\space)]
          [else
           (print ch)])
        (text-color Default)
        (text-background Default) )))
)

(define (draw-cursor)
  (gotoxy (+ (cursor-x (current-cursor)) text-x-pad sidebar-width)
          (+ (cursor-y (current-cursor)) text-y)) )

(define (buffer-name txt)
  (if (text-filename txt)
      (list->string
        (reverse
          (string->list
            (substring
              (list->string (reverse (string->list (text-filename txt))))
              0 (min (string-length (text-filename txt)) (- max-width 20))  ))))
      "New"))

(define (draw-footer)
  (gotoxy 0 max-height)
  (clreol)
  (text-color Yellow)
  (text-background Blue)

  (cond
    [(eqv? current-kmap normal-kmap)
     (print "NORMAL")]
    [(eqv? current-kmap insert-kmap)
     (print "--INSERT--")]
    [(eqv? current-kmap visual-line-kmap)
     (print "--VISUAL LINE--")]
  )
  (if (text-modified? (current-text))
      (print "    ")
      (print "[+] "))
  (print (buffer-name (current-text)))
  (clreol)
)

(define (draw-ui)
  (editor-update (current-editor))
  (draw-text)
  (draw-footer)
  (draw-cursor)
)

(clrscr)
(redraw-screen)

(draw-ui)

(view-draw (current-view))

(define (char->symbol ch)
  (string->symbol (string (char-downcase ch))))

(define quit? #f)

(define (handle-keypress mods key)
  (let* ([mod
          (if (not (null? mods))
              (list (car mods))
              '())]
         [key
          (if (char? key)
              (char->symbol key)
              key)])
    (let-values ([(kb matched) (match-keybind current-kmap (append keys-buf mod (list key)))])
      (if (not (null? matched))
          (set! keys-buf (append keys-buf mod (list key)))
          (set! keys-buf '()))
      (if kb
          (begin
            ((keybind-func kb))
            (set! keys-buf '())
            #t)
          #f))))

(let loop ()
  (update-screen)
  (let ((ev (read-event)))
    (when (key-press-event? ev)
      (cond
        [(handle-keypress (enum-set->list (keyboard-event-mods ev))
                          (keyboard-event-key ev))]
        [(and (or (enum-set=? (keyboard-event-mods ev) (modifier-set shift))
		  (not (keyboard-event-has-modifiers? ev)))
              (keyboard-event-char ev))
         (insert-char (keyboard-event-char ev))])

      (draw-ui))
    (when (not quit?)
      (loop))))

(restore-console)
