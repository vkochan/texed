#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 vkochan
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (text-mode console)
        (text-mode console events)
        (text-mode termios)
        (text-edit text)
        (text-edit view)
        (text-edit editor)
        (text-edit keymap))

(define max-height 10)
(define max-width 80)
(define text-x-pad 1)
(define text-y 1)

(define ed (make-editor max-width max-height))

(define curs (editor-cursor ed))
(define txt (editor-text ed))
(define vw (editor-view ed))
(define ch-tbl (make-char-table
'(
  (#\tab (width 4) (fill #\space))
)))

(define sidebar-width 2)

(view-set-char-table vw ch-tbl)
(view-enable-line-num vw #t)

(define (move-left)
  (editor-move-cursor-left ed))

(define (move-right)
  (editor-move-cursor-right ed))

(define (move-down)
  (editor-move-cursor-down ed))

(define (move-up)
  (editor-move-cursor-up ed))

(define (delete-prev-char)
  (editor-delete-char ed))

(define (insert-char ch)
  (editor-insert-char ed ch))

(define (debug m)
  (let ([x (wherex)]
        [y (wherey)])
    (gotoxy 0 (window-maxy))
    (print m)
    (gotoxy x y) ))

(define (fill-line x y ch)
  (let loop ([x x] [n (- (window-maxx) x)])
    (when (> n 0)
      (gotoxy x y)
      (print ch)
      (loop (+ x 1) (- n 1)) )))

(define (draw-header)
  (gotoxy 0 0)
  (clreol)
  (text-color White)
  (text-background Red)
  (print "Demo of texed library")
  (fill-line (wherex) (wherey) " ")
)

(define (draw-footer)
  (gotoxy 0 (+ max-height 1))
  (clreol)
  (text-color White)
  (text-background Red)

  (clreol)
  (print "len:")(print (text-size txt))
  (print ", pos:")(print (cursor-pos curs))
  (print ", start:")(print (view-start-pos vw))
  (print ", end:")(print (view-end-pos vw))
  (print ", line:")(print (cursor-y curs))
  (print ", col:")(print (cursor-x curs))
  (fill-line (wherex) (wherey) " ")
  
  (gotoxy 0 (+ max-height 2))
  (clreol)
  (print "[q:Exit] [C-d:Scroll down] [C-u:Scroll up] [PageUp:Scroll page up] [PageDown:Scroll page down]")
  (fill-line (wherex) (wherey) " ")

  (gotoxy 0 (+ max-height 3))
  (clreol)
  (print "[Alt-h:Cursor left] [Alt-j:Cursor down] [Alt-k:Cursor up] [Alt-l:Cursor left]")
  (fill-line (wherex) (wherey) " ")
)

(define (line-num-width)
   (let ([num (text-line-num txt (text-size txt))])
     (string-length (number->string num))))

(define (draw-text)
  (define prev-line-num #f)

  (set! sidebar-width (+ (line-num-width) 1))
  (text-color Green)
  (text-background Black)
  
  (gotoxy 0 text-y)

  (view-for-each-line vw
    (lambda (l)
      (gotoxy 0 (+ text-y (line-row l)))
      (clreol)
      (when (not (eq? prev-line-num (line-num l)))
        (set! prev-line-num (line-num l))
        (print (number->string (line-num l))) )
      (gotoxy (- sidebar-width 1) (+ text-y (line-row l)))
      (print "|") ))

  (view-for-each-cell vw
    (lambda (line col cell pos)
      (gotoxy (+ col text-x-pad sidebar-width) (+ (line-row line) text-y))
      (let ([ch (cell-char cell)])
        (cond
          [(eqv? ch #\newline)
           (print #\space)]
          [else
           (print ch)])
      ))))

(define (draw-cursor)
  (gotoxy (+ (cursor-x curs) text-x-pad sidebar-width) (+ (cursor-y curs) text-y)) )

(define (draw-ui)
  (editor-update ed)
  (draw-header)
  (draw-text)
  (draw-footer)
  (draw-cursor)
)

(clrscr)
(redraw-screen)

(draw-ui)

(view-draw vw)

(define (char->symbol ch)
  (string->symbol (string (char-downcase ch))))

(define quit? #f)

(define kmap (make-keymap))
(bind-key kmap '(alt q) (lambda () (set! quit? #t)))
(bind-key kmap '(Backspace) delete-prev-char)
(bind-key kmap '(alt h) move-left)
(bind-key kmap '(alt j) move-down)
(bind-key kmap '(alt k) move-up)
(bind-key kmap '(alt l) move-right)
(bind-key kmap '(PageUp) (lambda () (view-scroll-page-up vw)))
(bind-key kmap '(PageDown) (lambda () (view-scroll-page-down vw)))

(define (handle-keypress mods key)
  (let* ([mod
          (if (not (null? mods))
              (list (car mods))
              '())]
         [key
          (if (char? key)
              (char->symbol key)
              key)])
    (let ([kb (lookup-keybind kmap (append mod (list key)))])
      (if kb
          (begin
            ((keybind-func kb))
            #t)
          #f))))

(let loop ()
  (update-screen)
  (let ((ev (read-event)))
    (when (key-press-event? ev)
      (cond
        [(handle-keypress (enum-set->list (keyboard-event-mods ev))
                          (keyboard-event-key ev))]
        [(and (not (keyboard-event-has-modifiers? ev))
              (keyboard-event-char ev))
         (insert-char (keyboard-event-char ev))])
         
      (gotoxy 0 (+ max-height 4))
      (clreol)
      (print (keyboard-event-key ev))
      (draw-ui))
    (when (not quit?)
      (loop))))

(restore-console)
