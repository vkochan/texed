;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-edit view)
  (export
    make-char-table
    make-view
    view-default-cell
    view-set-default-cell
    view-enable-line-numbering
    view-line-numbering-enabled?
    view-set-dirty
    view-dirty?
    view-resize
    view-set-start-pos
    view-start-pos
    view-set-end-pos
    view-end-pos
    view-set-char-table
    view-height
    view-width
    view-draw
    view-for-each-cell
    view-for-each-line
    view-scroll-down
    view-scroll-up
    view-scroll-page-up
    view-scroll-page-down
    view-at
    view-at-char
    view-cell
    line-number
    line-row
    line-pos
    line-width
    make-cell
    cell-char
    cell-set-char
    cell-width
    cell-set-width
    cell-fg-color
    cell-set-fg-color
    cell-bg-color
    cell-set-bg-color
    cell-style
    cell-set-style)
  (import
    (rnrs)
    (text-edit text)
    (text-mode unicode))

(define-record-type cell
  (fields
    (mutable char cell-char cell-set-char)
    (mutable width cell-width cell-set-width)
    (mutable fg-color cell-fg-color cell-set-fg-color)
    (mutable bg-color cell-bg-color cell-set-bg-color)
    (mutable style cell-style cell-set-style))
  (protocol
    (lambda (new)
      (lambda (fg bg style)
        (new #\space 1 fg bg style) ))))

(define-record-type line
  (fields
    (mutable width)
    (mutable row)
    (mutable pos)
    (mutable cells)
    (mutable has-nl?)
    (mutable number))
  (protocol
    (lambda (new)
      (lambda ()
        (new 1 0 #f #f #f #f) ))))

(define-record-type char-attr
  (fields
    (mutable width)
    (mutable char)
    (mutable fg)
    (mutable bg))
  (protocol
    (lambda (new)
      (lambda ()
        (new #f #f #f #f) ))))

(define-record-type char-table
  (fields
    ht)
  (protocol
    (lambda (new)
      (lambda (tbl)
        (let ([ht (make-eqv-hashtable)])
          (for-each (lambda (lst)
                      (let ([attr (make-char-attr)])
                        (for-each (lambda (a)
                                    (case (car a)
                                      [(fill) (char-attr-char-set! attr (cadr a))]
                                      [(width) (char-attr-width-set! attr (cadr a))]
                                      [(fg) (char-attr-fg-set! attr (cadr a))]
                                      [(bg) (char-attr-bg-set! attr (cadr a))]
                                      [else (error 'make-char-table "Invalid property name")]))
                          (cdr lst))
                        (hashtable-set! ht (car lst) attr) ))
            tbl)
          (new ht) ) ))))

(define (make-lines width height)
  (let ([lines (make-vector height)])
    (let loop-lines ([row 0])
      (when (< row (vector-length lines))
        (let ([line (make-line)])
          (line-cells-set! line (make-vector width))
          (vector-set! lines row line)
          (let loop-cols ([col 0])
            (when (< col width)
              (vector-set! (line-cells line) col (make-cell #f #f #f))
              (loop-cols (+ col 1))))
          (loop-lines (+ row 1)) )))
    lines))

(define-record-type view
  (fields
    text
    (mutable lines)
    (mutable start-pos view-start-pos view-set-start-pos)
    (mutable end-pos view-end-pos view-set-end-pos)
    (mutable default-cell view-default-cell view-set-default-cell)
    (mutable height)
    (mutable width)
    (mutable cursor)
    (mutable char-table view-char-table view-set-char-table)
    (mutable last-row)
    (mutable line-numbering-enabled? view-line-numbering-enabled? view-enable-line-numbering)
    (mutable dirty? view-dirty? view-set-dirty))
  (protocol
    (lambda (new)
      (lambda (txt width height)
        (let ([lines (make-lines width height)])
          (new txt ;; text
               lines ;; lines
               0 ;; start-pos
               0 ;; end-pos
               #f ;; default-cell
               height ;; height
               width ;; width
               (make-cursor txt) ;; cursor
               #f ;; char-table
               0 ;; last-row
               #f ;; line-numbering-enabled?
               #t ;; dirty?
               ))))))

(define (view-resize view width height)
  (view-lines-set! view (make-lines width height))
  (view-width-set! view width)
  (view-height-set! view height)
  (view-draw view))

(define (view-draw view)

  (define (char-attrs char)
    (let ([char-tbl (view-char-table view)]
          [width 0]
          [attr #f]
          [fg #f]
          [bg #f])
      (when char-tbl
        (set! attr (hashtable-ref (char-table-ht char-tbl) char #f)))
      (when attr
        (set! width (or (char-attr-width attr) width))
        (set! char (or (char-attr-char attr) char))
        (set! fg (char-attr-fg attr))
        (set! bg (char-attr-bg attr)))
      (when (= width 0)
        (set! width (char-width char))
        (when (= width 0)
          (set! width 1)))
      (values char width fg bg)))

  (define (fill-cell cell char fg bg)
    (let ([def-cell (view-default-cell view)])
      (when def-cell
            (cell-set-fg-color cell (cell-fg-color def-cell))
            (cell-set-bg-color cell (cell-bg-color def-cell))
            (cell-set-style cell (cell-style def-cell)))
      (when fg
            (cell-set-fg-color cell fg))
      (when bg
            (cell-set-bg-color cell bg))
      (cond [char (cell-set-char cell char)]
            [else (cell-set-char cell #\space)])))

  (define start-pos (view-start-pos view))
  (define end-pos (view-start-pos view))
  (define curs (view-cursor view))
  (define lines (view-lines view))
  (define txt (view-text view))
  (define enable-eof? 0)

  (define line-num (text-line-number txt start-pos))

  (view-set-dirty view #f)

  (cursor-update curs)
  (cursor-to curs start-pos)

  (let loop-lines ([lineno 0])
    (when (< lineno (vector-length lines))
      (let* ([line (vector-ref lines lineno)]
             [col 0]
             [cols-count (vector-length (line-cells line))])
        (line-number-set! line #f)
        (line-pos-set! line end-pos)
        (line-row-set! line lineno)
        (line-has-nl?-set! line #f)
        (line-width-set! line 0)
        (let loop-chars ([char (text-char curs)])
          (when (and (< end-pos (text-size txt))
                     (< col cols-count)
                     char)
            (let ([cell (vector-ref (line-cells line) col)]
                  [nl? (eq? char #\newline)])
              (cell-set-width cell 0)
              (when nl?
                (line-has-nl?-set! line #t))
              (let-values ([(char width fg bg) (char-attrs char)])
                (when (>= (- cols-count col) width)
                  (view-last-row-set! view lineno)
                  (cell-set-width cell width)
                  (line-width-set! line (+ (line-width line) 1))
                  (set! end-pos (+ end-pos 1))
                  (fill-cell cell char fg bg)
                  (if (> width 1)
                      (let skip ([num (- width 1)]
                                 [cell (vector-ref (line-cells line) (+ col 1))])
                        (when (positive? num)
                          (cell-set-width cell 0)
                          (fill-cell cell #f fg bg)
                          (set! col (+ col 1))
                          (skip (- num 1) (vector-ref (line-cells line) (+ col 1))))))
                  (set! col (+ col 1))
                  (when (and (cursor-to-next-char curs)
                        (not nl?))
                    (loop-chars (text-char curs))))))))
        (let fill-up ([col col])
          (when (< col cols-count)
            (let ([cell (vector-ref (line-cells line) col)])
              (cell-set-width cell 0)
              (fill-cell cell #f #f #f)
              (fill-up (+ col 1)))))
    (when (and (view-line-numbering-enabled? view)
               (positive? (line-width line)))
      (line-number-set! line line-num)
      (when (line-has-nl? line)
        (set! line-num (+ line-num 1))))
    (loop-lines (+ lineno 1)))))
  (view-set-end-pos view (- end-pos enable-eof?)))

(define (view-for-each-line view fn)
  (define lines (view-lines view))

  (let loop-lines ([lineno 0])
    (when (< lineno (vector-length lines))
      (let ([line (vector-ref lines lineno)])
        (fn line))
      (loop-lines (+ lineno 1)))))

;;(define view-for-each-cell
;;  (case-lambda
;;    [(view fn)
;;     (view-for-each-cell view (view-start-pos view) (view-end-pos view) fn)]
;;
;;    [(view start-pos end-pos fn)
;;  
;;     (define lines (view-lines view))
;;
;;     (let loop-lines ([lineno 0])
;;       (when (< lineno (vector-length lines))
;;         (let loop-cols ([col 0])
;;           (let* ([line (vector-ref lines lineno)])
;;             (when (< col (vector-length (line-cells line)))
;;               (fn line col (vector-ref (line-cells line) col))
;;               (loop-cols (+ col 1)))))
;;         (loop-lines (+ lineno 1))))]
;;))

(define view-for-each-cell
  (case-lambda
    [(view fn)
     (view-for-each-cell view (view-start-pos view) (view-end-pos view) fn)]

    [(view start-pos end-pos fn)
  
     (define lines (view-lines view))

     (when (not (eqv? (view-start-pos view) (view-end-pos view)))
       (let loop-lines ([lineno 0][cur-pos (view-start-pos view)])
         (when (and (< lineno (vector-length lines))
                    (<= cur-pos end-pos))
           (let ([line (vector-ref lines lineno)])
             (if (and (> (+ cur-pos (line-width line)) start-pos)
                      (<= cur-pos end-pos))
                 (begin
                   (let loop-cols ([col 0][cur-pos cur-pos])
                     (when (and (< col (vector-length (line-cells line)))
                                (<= cur-pos end-pos))
                       (let ([cell (vector-ref (line-cells line) col)])
                         (when (>= cur-pos start-pos)
                           (fn line col cell cur-pos))
                         (loop-cols (+ col 1) (+ (cell-width cell) cur-pos)))))
                   (loop-lines (+ lineno 1) (+ (line-width line) cur-pos)) )
                 (loop-lines (+ lineno 1) (+ (line-width line) cur-pos))) ))))]
))

(define (view-scroll-down view n)
  (define start-pos (view-start-pos view))
  (define curs (view-cursor view))

  (cursor-update curs)
  (cursor-to curs start-pos)

  (let loop ([n n])
    (if (and (> n 0)
             (cursor-to-line-end curs)
             (cursor-to curs (+ (cursor-pos curs) 1)))
        (loop (- n 1)) ))

  (view-set-start-pos view (cursor-pos curs))
  (view-draw view))

(define (view-scroll-up view n)
  (define start-pos (view-start-pos view))
  (define curs (view-cursor view))

  (cursor-update curs)
  (cursor-to curs start-pos)

  (let loop ([n n])
    (if (and (> n 0)
             (cursor-to-prev-char curs #\newline) )
        (loop (- n 1)) ))

  (cursor-to-line-begin curs)
  (view-set-start-pos view (cursor-pos curs))
  (view-draw view))

(define (view-scroll-page-down view)
  (define end-pos (view-end-pos view))
  (define curs (view-cursor view))

  (cursor-update curs)
  (cursor-to curs (+ end-pos 1))

  (cursor-to-line-begin curs)
  (view-set-start-pos view (cursor-pos curs))
  (view-draw view))

(define (view-scroll-page-up view)
  (view-scroll-up view (view-height view)))

(define (view-at view pos)
  (cond
    [(or (< pos (view-start-pos view))
         (> pos (view-end-pos view)))
     (values #f #f #f)]
    [(and (= pos (view-end-pos view)) (= (view-end-pos view) 0))
     ;; TODO this is a workaround instead of fixing a real problem
     (values 0 0 0)]
    [else
     (let ([line (vector-ref (view-lines view) 0)]
           [curr-pos (view-start-pos view)]
           [curr-line 0]
           [curr-col 0])
       (let loop-lines ()
         (when (and (< curr-line (view-height view))
                    (< curr-line (view-last-row view))
                    (<= (+ curr-pos (line-width line))
                        pos))
           (set! curr-pos (+ curr-pos (line-width line)))
           (when (< curr-line (- (view-height view) 1))
             (set! curr-line (+ curr-line 1))
             (set! line (vector-ref (view-lines view) curr-line))
             (loop-lines))))
       (let loop-cells ([col 0])
         (set! curr-col col)
         (when (and (< curr-pos pos)
                    (< col (view-width view)))
           (let ([cell (vector-ref (line-cells line) col)])
             (set! curr-pos (+ curr-pos 1))
             (set! curr-col col)
             (loop-cells (+ col (cell-width cell))))))
       ;; handle some corner cases
       (cond [(or (= curr-col (view-width view))
                  (and (= curr-pos (view-end-pos view))
                       (line-has-nl? line))
                  (and (= curr-pos (text-size (view-text view)))
                       (line-has-nl? line)))
              (set! curr-line (+ curr-line 1))
              (set! curr-col 0)])
       (values curr-line curr-col pos))
    ]))

(define (view-at-char view lineno col)
  (cond
    [(or (>= lineno (view-height view))
         (>= col (view-width view)))
     (values #f #f #f)]
    [else
     (set! lineno (min lineno (view-last-row view)))
     (let ([line (vector-ref (view-lines view) lineno)])
       (let loop ([num 0] [offs 0])
         (let ([cell (view-cell view lineno num)])
           (if (or (>= (+ offs 1)
                       (line-width line))
                   (> (+ num (max (cell-width cell) 1))
                      col))
               (values lineno num (+ (line-pos line) offs))
             (loop (+ num (max (cell-width cell) 1))
                   (+ offs 1))
           ))))]))

(define (view-cell view lineno col)
  (if (>= lineno (view-height view))
      (error 'view-cell "Invalid line number"))
  (if  (>= col (view-width view))
      (error 'view-cell "Invalid column number"))
  (vector-ref (line-cells (vector-ref (view-lines view) lineno))
              col))

)
