;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-edit keymap)
  (export
    make-keymap
    keymap-bindings
    bind-key
    keybind-func
    keybind-keys
    keybind-desc
    lookup-keybind
    match-keybind)
  (import (rnrs (6))
          (rnrs mutable-pairs))

(define-record-type keybind
  (fields
    keys
    func
    desc))

(define-record-type $keymap
  (fields
    (mutable parent)
    (mutable keybinds)))

(define make-keymap
  (case-lambda
    [()
     (make-keymap global-keymap)]

    [(parent)
     (make-$keymap parent '())]))

(define (keymap-bindings map)
  ($keymap-keybinds map))

(define (lookup-keybind kmap keys)
  (let loop ([kbinds ($keymap-keybinds kmap)])
    (if (null? kbinds)
        (if ($keymap-parent kmap)
            (lookup-keybind ($keymap-parent kmap) keys)
            #f)
        (let ([kb (car kbinds)])
          (if (equal? (keybind-keys kb) keys)
              kb
              (loop (cdr kbinds)))))))

(define (list-has-prefix? lst pref)
  (let loop ([lst lst] [pref pref])
    (if (or (null? lst) (null? pref))
      (null? pref)
      (if (not (eqv? (car lst) (car pref)))
        #f
        (loop (cdr lst) (cdr pref))))))

(define (match-keybind kmap keys)
  (let loop ([kbinds ($keymap-keybinds kmap)] [exact #f] [matched '()])
    (if (null? kbinds)
        (if ($keymap-parent kmap)
            (let-values ([(pexact pmatched) (match-keybind ($keymap-parent kmap) keys)])
              (values (or exact pexact) (append pmatched matched)))
            (values exact matched))
        (let ([kb (car kbinds)])
          (if (list-has-prefix? (keybind-keys kb) keys)
              (loop (cdr kbinds)
                    (if (eqv? (length (keybind-keys kb)) (length keys))
                        kb
                        exact)
                    (append matched (list kb)))
              (loop (cdr kbinds) exact matched))))))

(define (list-set! list k val)
  (if (zero? k)
      (set-car! list val)
      (list-set! (cdr list) (- k 1) val)))

(define bind-key
  (case-lambda
    [(keys func)
     (bind-key global-keymap keys func)]

    [(kmap keys func)
     (bind-key kmap keys #f func)]

    [(kmap keys desc func)
     (let* ([keys (map (lambda (k)
                         (if (number? k) (string->symbol (number->string k))
                             k))
                       keys)]
            [new-kb (make-keybind keys func desc)])
       (let loop ([kbinds ($keymap-keybinds kmap)] [i 0])
         (if (null? kbinds)
             ($keymap-keybinds-set! kmap (append ($keymap-keybinds kmap)
                                                 (list new-kb)))
             (let ([kb (list-ref ($keymap-keybinds kmap) i)])
               (if (equal? keys (keybind-keys kb))
                   (list-set! ($keymap-keybinds kmap) i new-kb)
                   (loop (cdr kbinds) (+ i 1)))))))]))

(define global-keymap (make-keymap #f))

)
