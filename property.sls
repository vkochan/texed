;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-edit property)
  (export
    make-text-property-list
    text-property-list-length
    make-text-property
    add-text-property
    delete-text-property
    get-n-text-property
    for-each-text-property
    text-property=?
    text-property-start
    text-property-end
    text-property-name
    text-property-data)
  (import
    (rnrs (6)))

(define-record-type text-property-list
  (fields
    (mutable head)
    (mutable length))
  (protocol
    (lambda (new)
      (lambda ()
        (new #f 0)))))

(define-record-type text-property
  (fields
    (mutable name)
    (mutable start)
    (mutable end)
    (mutable data)
    (mutable prev)
    (mutable next))
  (protocol
    (lambda (new)
      (letrec ([mk-property
                (case-lambda
                  [(start end data)
                   (new #f start end data #f #f)]
                  [(start end name data)
                   (new name start end data #f #f)
                  ] )])
        mk-property))))

(define (add-text-property pl p)
  (define (property>? p1 p2)
    (> (text-property-start p1) (text-property-start p2)))

  (define (find-prev pl p)
    (let loop ([prev #f][head (text-property-list-head pl)])
      (if (not head)
          prev
          (if (property>? p head)
              (loop head (text-property-next head))
              prev))))

  (let* ([prev (find-prev pl p)]
         [next (if prev
                   (text-property-next prev)
                   (text-property-list-head pl))])
    (when prev
      (text-property-next-set! prev p))
    (text-property-prev-set! p prev)
    (when next
      (text-property-prev-set! next p))
    (text-property-next-set! p next)
    (when (not prev)
      (text-property-list-head-set! pl p))
    (text-property-list-length-set! pl (+ (text-property-list-length pl) 1)) ))

(define (text-property-in-range? p start end)
  (and (<= start (text-property-end p)) (>= end (text-property-start p))))

(define (delete-text-property pl start end)
  (define (delete-from-list pl p)
    (when (not (text-property-list-head pl))
      (text-property-list-head-set! pl (text-property-next p)))
    (let ([prev (text-property-prev p)] [next (text-property-next p)])
      (if prev
          (text-property-next-set! prev next)
          (text-property-list-head-set! pl next))
      (if next
          (text-property-prev-set! next prev)))
    (text-property-list-length-set! pl (- (text-property-list-length pl) 1)))

  (let loop ([p (text-property-list-head pl)])
    (when (and p (<= (text-property-start p) end))
      (let ([next (text-property-next p)])
        (when (text-property-in-range? p start end)
          (delete-from-list pl p))
        (loop next) ))))

(define (for-each-text-property pl start end fn)
  (let loop ([p (text-property-list-head pl)])
    (when (and p (<= (text-property-start p) end))
      (when (text-property-in-range? p start end)
        (fn p))
      (loop (text-property-next p)))))

(define (get-n-text-property pl n)
  (let loop ([i 0][curr (text-property-list-head pl)])
    (if (or (>= i n) (and (< i n) (not curr)))
        curr
        (loop (+ i 1) (text-property-next curr)))))

(define (text-property=? p1 p2)
  (and
    (equal? (text-property-name p1) (text-property-name p2))
    (eqv? (text-property-start p1) (text-property-start p2))
    (eqv? (text-property-end p1) (text-property-end p2))
    (equal? (text-property-data p1) (text-property-data p2)) ))

)
