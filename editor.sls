;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-edit editor)
  (export
    make-editor
    editor-set-var
    editor-get-var
    editor-text
    editor-view
    editor-mark
    editor-cursor
    editor-update
    editor-move-cursor-right
    editor-move-cursor-left
    editor-move-cursor-down
    editor-move-cursor-up
    editor-scroll-page-down
    editor-scroll-page-up
    editor-delete-char
    editor-delete-line
    editor-delete-selection
    editor-insert-char
    editor-insert-string
    editor-undo
    editor-redo
    editor-selected-range)
  (import (rnrs (6))
          (text-edit text)
          (text-edit view))

(define-record-type text-editor
  (fields
    (immutable text editor-text)
    (immutable view editor-view)
    (immutable cursor editor-cursor)
    (immutable mark editor-mark)
    (immutable vars)))

(define make-editor
  (case-lambda
    [(width height)
     (make-editor width height #f)]

    [(width height file)
     (let* ((text (make-text file))
            (view (make-view text width height))
            (curs (make-cursor text))
            (mark (make-cursor text)))
       (make-text-editor text view curs mark (make-eqv-hashtable)))]))

(define (editor-set-var ed sym val)
  (hashtable-set! (text-editor-vars ed) sym val) )

(define editor-get-var
  (case-lambda
    [(ed sym)
     (editor-get-var ed sym #f)]

    [(ed sym def)
     (hashtable-ref (text-editor-vars ed) sym def)] ))

(define (editor-update ed)
  (define curs (editor-cursor ed))
  (define vw (editor-view ed))

  (when (or (view-dirty? vw)
            (< (cursor-pos curs) (view-start-pos vw))
            (> (cursor-pos curs) (view-end-pos vw)))
    (view-draw vw))
  (when (> (cursor-pos curs) (view-end-pos vw))
    (view-scroll-down vw 1))
  (when (< (cursor-pos curs) (view-start-pos vw))
    (view-scroll-up vw 1))
  (when (or (< (cursor-pos curs) (view-start-pos vw))
            (> (cursor-pos curs) (view-end-pos vw)))
    (cursor-to curs (view-start-pos vw)))
  (let-values ([(y x p) (view-at vw (cursor-pos curs))])
    (when (and y x)
      (when (= y (view-height vw))
        (view-scroll-down vw 1)
        (let-values ([(new-y new-x new-p) (view-at vw (cursor-pos curs))])
          (set! x new-x)
          (set! y new-y)))
      (when (and x y)
        (cursor-set-x curs x)
        (cursor-set-y curs y)))))

(define (editor-move-cursor-left ed)
  (define curs (editor-cursor ed))
  (define vw (editor-view ed))

  (when (> (cursor-pos curs) 0)
    (cursor-to-prev-char curs)
    (when (< (cursor-pos curs) (view-start-pos vw))
      (view-scroll-up vw 1) )))

(define (editor-move-cursor-right ed)
  (define curs (editor-cursor ed))
  (define text (editor-text ed))
  (define vw (editor-view ed))

  (when (< (cursor-pos curs) (text-size text))
    (cursor-to-next-char curs)
    (when (> (cursor-pos curs) (view-end-pos vw))
      (view-scroll-down vw 1) )))

(define (editor-move-cursor-down ed)
  (define curs (editor-cursor ed))
  (define vw (editor-view ed))

  (let-values ([(l c p) (view-at vw (cursor-pos curs))])
    (if (and l (= l (- (view-height vw) 1)))
        (begin
          (view-scroll-down vw 1)
          (let-values ([(l2 c2 p2) (view-at-char vw l c)])
            (when l2
              (cursor-to curs p2) )))
      (let-values ([(l2 c2 p2) (view-at-char vw (+ l 1) c)])
        (when l2
          (cursor-to curs p2)))
    )))

(define (editor-move-cursor-up ed)
  (define curs (editor-cursor ed))
  (define vw (editor-view ed))

  (let-values ([(l c p) (view-at vw (cursor-pos curs))])
    (if (and l (= l 0))
        (begin
          (view-scroll-up vw 1)
          (let-values ([(l2 c2 p2) (view-at-char vw l c)])
            (when l2
              (cursor-to curs p2) )))
      (let-values ([(l2 c2 p2) (view-at-char vw (- l 1) c)])
        (when l2
          (cursor-to curs p2)))
    )))

(define (editor-scroll-page-down ed)
  (view-scroll-page-down (editor-view ed)))

(define (editor-scroll-page-up ed)
  (view-scroll-page-up (editor-view ed)))

(define (editor-delete-char ed)
  (define curs (editor-cursor ed))
  (define text (editor-text ed))

  (when (> (cursor-pos curs) 0)
    (cursor-to-prev-char curs)
    (text-delete text (cursor-pos curs) 1)
    (cursor-update curs)
    (view-set-dirty (editor-view ed) #t)))

(define (editor-delete-line ed)
  (define curs (editor-cursor ed))
  (define text (editor-text ed))

  (define begin (cursor-line-begin-pos curs))
  (define end (with-saved-cursor curs
                (cursor-to-line-end curs)
                (cursor-to-next-char curs)
                (cursor-pos curs)))

  (when (and (positive? (fx- end begin))
             (positive? (text-size text)))
    (cursor-to-line-begin curs)
    (cursor-to-prev-char curs)
    (cursor-to-line-begin curs)
    (text-delete text begin (- end begin))
    (cursor-update curs)
    (view-set-dirty (editor-view ed) #t)))

(define (editor-delete-selection ed)
  (let-values ([(start-pos end-pos) (editor-selected-range ed)])
    (text-delete (editor-text ed) start-pos (+ (- end-pos start-pos) 1))
    (cursor-update (editor-cursor ed))
    (view-set-dirty (editor-view ed) #t)))

(define (editor-insert-char ed ch)
  (define curs (editor-cursor ed))
  (define text (editor-text ed))

  (text-insert text (make-string 1 ch) (cursor-pos curs))
  (cursor-update curs)
  (cursor-to-next-char curs)
  (view-set-dirty (editor-view ed) #t))

(define (editor-insert-string ed str)
    (text-insert (editor-text ed) str (cursor-pos (editor-cursor ed)))
    (cursor-update (editor-cursor ed))
    (view-set-dirty (editor-view ed) #t))

(define (editor-undo ed)
  (let ([pos (text-undo (editor-text ed))])
    (when pos
      (cursor-to (editor-cursor ed) pos)
      (cursor-update (editor-cursor ed))
      (view-set-dirty (editor-view ed) #t))))

(define (editor-redo ed)
  (let ([pos (text-redo (editor-text ed))])
    (when pos
      (cursor-to (editor-cursor ed) pos)
      (cursor-update (editor-cursor ed))
      (view-set-dirty (editor-view ed) #t))))

(define (editor-selected-range ed)
  (values (min (cursor-pos (editor-cursor ed)) (cursor-pos (editor-mark ed)))
          (max (cursor-pos (editor-cursor ed)) (cursor-pos (editor-mark ed))) ))

)
